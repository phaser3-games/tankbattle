/**
 * 定義一個 scene，用成員變數儲存 scene 上面的物件
 * override preload, create, update 函式
 */
class Scene1 extends Phaser.Scene {
    constructor() {
        super();
        this.player = null;
        this.platforms = null;
        this.cursors = null;
        this.stars = null;
        this.lastFire = -1;
        this.lastCanonFire=-1;
        this.lastEnemyFire=-1;
    }
    preload() {
        this.load.image('tank', 'assets/tank2-export1.png');
        this.load.image('tiles', 'assets/overworld_tileset_grass.png');
        this.load.tilemapTiledJSON('map', 'assets/level1.json');
        this.load.image('tree', 'assets/tile070.png');
        this.load.image('bullet', 'assets/rocket.png');
        this.load.image('cannon', 'assets/tile199.png');
        this.load.image('enemy', 'assets/tank3-export8.png');
    }
    create() {
        const map = this.make.tilemap({ key: 'map' });
        // Add the tileset to the map so the images would load correctly in Phaser
        const tileset = map.addTilesetImage('roads', 'tiles');
        this.platforms = map.createStaticLayer('background', tileset, 0, 0);
        this.platforms.setCollisionByExclusion(31, true);

        this.player = new Player(this, 50, 600);
        this.physics.add.collider(this.player, this.platforms);

        this.trees = map.createFromObjects('trees', 71, { key: 'tree' });
        let self = this;
        this.trees.forEach(tree => {
            self.physics.world.enable(tree);
            tree.body.setCollideWorldBounds(true);
            tree.body.setImmovable(true);
        });
        this.physics.add.collider(this.player, this.trees);


        this.cannons = map.createFromObjects('enemyCannon', 200, { key: 'cannon' });
        this.cannons.forEach(cannon => {
            self.physics.world.enable(cannon);
            cannon.body.setCollideWorldBounds(true);
            cannon.body.setImmovable(true);
        });
        this.canonBullets=[];
        this.cursors = this.input.keyboard.createCursorKeys();

        this.bullets = [];
        

        this.physics.add.collider(this.bullets, this.trees, function (b, t) {
            b.disableBody(true, true);
            t.destroy();
        });
        this.physics.add.collider(this.bullets, this.platforms, function (b, p) {
            b.disableBody(true, true);
        });
        this.physics.add.collider(this.bullets, this.canonBullets, function (b, c) {
            b.disableBody(true, true);
            c.disableBody(true, true);
        });
        this.physics.add.collider(this.player, this.canonBullets, function (p, c) {
            p.disableBody(true, true);
            c.disableBody(true, true);
        });

        this.enemyBullets=[];
        this.physics.add.collider(this.enemyBullets, this.trees, function (b, t) {
            b.disableBody(true, true);
            t.destroy();
        });
        this.physics.add.collider(this.enemyBullets, this.platforms, function (b, p) {
            b.disableBody(true, true);
        });
        this.physics.add.collider(this.player, this.enemyBullets, function (p, c) {
            p.disableBody(true, true);
            c.disableBody(true, true);
        });
        this.enemy=new Enemy(this, 200, 20, this.player);
        this.enemy2=new Enemy(this,10, 300, this.player);
        this.enemy2.angle=180;
        this.enemies=[this.enemy, this.enemy2];
        this.physics.add.collider(this.enemies, this.bullets, function (p, c) {
            p.disableBody(true, true);
            c.disableBody(true, true);
        });
        this.physics.world.on('worldbounds', function(body){
            if(body.gameObject==self.enemy){
                self.enemy.doRotate=true;
            }
            if(body.gameObject==self.enemy2){
                self.enemy2.doRotate=true;
            }
        },this);

        this.physics.add.collider(this.enemies, this.trees, function(enemy){
            enemy.doRotate=true;
        });

        this.physics.add.collider(this.enemies, this.platforms, function(enemy){
            enemy.doRotate=true;
        });

        

    }


    update() {
        if (this.bullets && this.bullets.length > 0 &&
            (this.bullets[0].y < 0 || this.bullets[0].y > 640 || this.bullets[0].x < 0 ||
                this.bullets[0].x > 640 || !this.bullets[0].body.enable)) {
            this.bullets.shift();
        }
        if (this.canonBullets && this.canonBullets.length > 0 &&
            (this.canonBullets[0].y < 0 || this.canonBullets[0].y > 640 || this.canonBullets[0].x < 0 ||
                this.canonBullets[0].x > 640 || !this.canonBullets[0].body.enable)) {
            this.canonBullets.shift();
        }
        if (this.enemyBullets && this.enemyBullets.length > 0 &&
            (this.enemyBullets[0].y < 0 || this.enemyBullets[0].y > 640 || this.enemyBullets[0].x < 0 ||
                this.enemyBullets[0].x > 640 || !this.enemyBullets[0].body.enable)) {
            this.enemyBullets.shift();
        }
        this.player.setVelocityX(0);
        this.player.setVelocityY(0);
        if (this.cursors.left.isDown) {
            //this.player.setVelocityX(-160);
            this.player.rotateCounterClockwise();
        }
        else if (this.cursors.right.isDown) {
            //this.player.setVelocityX(160);
            this.player.rotateClockwise();
        }
        if (this.cursors.up.isDown) {
            // this.player.setVelocityY(-160);
            this.player.move();
        }
        else if (this.cursors.down.isDown) {
            // this.player.setVelocityY(160);
            this.player.stop();
        }
        let self = this;
        let current = Date.now();
        if (this.cursors.space.isDown) {
            if ((current - this.lastFire) > 500) {
                this.lastFire = current;
                let bullet = new Bullet(self, self.player.x, self.player.y);
                let v = self.physics.velocityFromAngle(self.player.angle - 90, 100);
                bullet.angle = self.player.angle;
                bullet.setVelocity(v.x, v.y);
                self.bullets.push(bullet);
            }
        }
        if((current-this.lastCanonFire)>2500){
            this.lastCanonFire=current;
            this.cannons.forEach(cannon => {
                let bullet = new Bullet(self, cannon.x, cannon.y);
                let v = self.physics.velocityFromAngle(cannon.angle - 90, 120);
                bullet.angle = cannon.angle;
                bullet.setVelocity(v.x, v.y);
                self.canonBullets.push(bullet);
            });
        }

        if((current-this.lastEnemyFire)>3000 || this.enemy.doRotate && ((current-this.lastEnemyFire)>500) ){
            this.lastEnemyFire=current;
            let bullet = new Bullet(self, self.enemy.x, self.enemy.y);
            let v = self.physics.velocityFromAngle(self.enemy.angle - 90, 100);
            bullet.angle = self.enemy.angle;
            bullet.setVelocity(v.x, v.y);
            self.enemyBullets.push(bullet);
        }

        if((current-this.lastEnemyFire)>3000 || this.enemy2.doRotate && ((current-this.lastEnemyFire)>500) ){
            this.lastEnemyFire=current;
            let bullet = new Bullet(self, self.enemy2.x, self.enemy2.y);
            let v = self.physics.velocityFromAngle(self.enemy2.angle - 90, 100);
            bullet.angle = self.enemy2.angle;
            bullet.setVelocity(v.x, v.y);
            self.enemyBullets.push(bullet);
        }

        this.player.update();
        this.enemy.update();
        this.enemy2.update();
    }
}