class Enemy extends Phaser.Physics.Arcade.Sprite{
    constructor(scene, x, y, player){
        super(scene, x, y, "enemy");
        this.player=player;
        this.angle=0;
        this.scene=scene;
        scene.physics.world.enable(this);
        scene.add.existing(this);
        this.setBounce(0.2);
        this.setCollideWorldBounds(true);
        
        this.doRotate=false;
        this.body.onWorldBounds = true;

        this.lastRotateDirection=-1;
        this.lastResolve={};
        this.continuousMoving=0;
        let self=this;
        setInterval(() => {
            if(Math.random()>0.5){
                self.lastRotateDirection=(-1)*self.lastRotateDirection;
            }
        }, 30000);
    }

    update(){
        this.setVelocity(0);

        if(this.doRotate){
            this.continuousMoving=0;
            this.angle=(this.angle+this.lastRotateDirection)%360;
            this.doRotate=false;
        }
        else{
            let resolveKey=""+this.x+":"+this.y;
            let resolveHistory=this.lastResolve[resolveKey];
            if(!resolveHistory){
                resolveHistory=[];
                resolveHistory.push(this.angle);
                this.lastResolve[resolveKey]=resolveHistory;
            }else{
                for(let angle of resolveHistory){
                    if(angle==this.angle){
                        //tested, so skip this angle
                        this.doRotate=true;
                        return;
                    }
                }
                resolveHistory.push(this.angle);
            }
            this.continuousMoving++;
            if(this.continuousMoving>150){
                this.doRotate=true;
                return;
            }
            let v=(this.scene.physics.velocityFromAngle(this.angle-90, 30));
            this.setVelocity(v.x, v.y);
        }
    }
}