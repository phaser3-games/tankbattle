class Player extends Phaser.Physics.Arcade.Sprite{
    constructor(scene, x, y){
        super(scene, x, y, "tank");
        this.angle=0;
        this.scene=scene;
        scene.physics.world.enable(this);
        scene.add.existing(this);
        this.setBounce(0.2);
        this.setCollideWorldBounds(true);
        this.rotateClockwiseFlag=false;
        this.rotateCounterClockwiseFlag=false;
        this.moveFlag=false;
        this.body.onWorldBounds = true;
    }

    rotateClockwise(){
        this.rotateClockwiseFlag=true;
        this.rotateCounterClockwiseFlag=false;
    }

    rotateCounterClockwise(){
        this.rotateClockwiseFlag=false;
        this.rotateCounterClockwiseFlag=true;
    }

    move(){
        this.moveFlag=true;
    }

    stop(){
        this.moveFlag=false;
    }

    update(){
        if(this.rotateClockwiseFlag){
            this.angle=(this.angle+1)%360;
        }
        if(this.rotateCounterClockwiseFlag){
            this.angle=(this.angle-1)%360;
        }
        this.rotateClockwiseFlag=false;
        this.rotateCounterClockwiseFlag=false;
        if(this.moveFlag){
            let v=(this.scene.physics.velocityFromAngle(this.angle-90, 30));
            this.setVelocity(v.x, v.y);
        }else{
            this.setVelocity(0);
        }
        this.moveFlag=false;
    }
}